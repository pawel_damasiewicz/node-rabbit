const amqp = require('amqplib/callback_api');
const logger = require('../services/logger');

let onConnect = (queue) => {
  return (error, connection) => {
    if (error) {
      throw error;
    }

    connection.createChannel(onCreateChannel(queue));
  }
}

let onCreateChannel= (queue) => {
  return (error, channel) => {
    if (error) {
      throw error;
    }

    channel.assertQueue(queue, {
      durable: false
    });

    logger.info('Waiting for messages.', { queue: queue });

    channel.consume(queue, (msg) => {
      logger.info('Received message', { msg: msg.content.toString(), queue: queue });
    }, {
      noAck: true
    });
  }
}

amqp.connect('amqp://localhost', onConnect('hello'));

