let amqp = require('amqplib/callback_api');

export class MessageQueue {
  private uri;

  constructor(uri) {
    this.uri = uri;
    amqp.connect(uri, this.onConnect());
  }

  function onConnect() {
    return (error, connection) => {
      if (error) {
        throw error;
      }

      connection.createChannel(this.onCreateChannel(this.queue, this.message)
  }
}
