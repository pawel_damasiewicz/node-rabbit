const express = require('express');
const morgan = require('morgan');
const sendMessage = require('./send');
const logger = require('../services/logger');
const morganMiddleware = require('../services/morganMiddleware');

const app = express()
const port = 3000

app.use(morganMiddleware);
app.use(express.json());


app.get('/', (req, res) => {
  message = JSON.stringify(req.body);

  sendMessage(message);

  res.send({message: 'Ok'});
})

app.listen(port, () => {
  logger.info(`Example app listening at http://localhost:${port}`)
})
