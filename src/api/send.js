const amqp = require('amqplib/callback_api');
const logger = require('../services/logger');

const onCreateChannel = (queue, message) => {
  return (error, channel) => {
    if (error) {
      throw error;
    }

    channel.assertQueue(queue, {
      durable: false
    });

    channel.sendToQueue(queue, Buffer.from(message));
    logger.info('Message sent', {msg: message});
  }
};

const onConnect = (timeout) => {
  return (error, connection) => {
    if (error) {
      throw error;
    }

    connection.createChannel(onCreateChannel('hello', message));

    setTimeout(() => {
      connection.close();
    }, timeout);
  };
};

const sendMessage = (message) => {
  amqp.connect('amqp://localhost', onConnect(50));
}

module.exports = sendMessage;
